package com.auto;

import com.auto.mapper.ClassApplicationMapper;
import com.auto.mapper.EquipmentRepairMapper;
import com.auto.mapper.StudentMapper;
import com.auto.pojo.dto.EquipmentRepairQueryDTO;
import com.auto.pojo.entity.EquipmentRepair;
import com.auto.pojo.vo.EquipmentRepairPageQueryVO;
import com.github.pagehelper.Page;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;

import javax.annotation.Resource;

@SpringBootTest
class AutoLabSystemApplicationTests {

    @Resource
    private StudentMapper studentMapper;

    @Resource
    private ClassApplicationMapper classApplicationMapper;

    @Resource
    private RedisTemplate redisTemplate;

    @Resource
    private EquipmentRepairMapper equipmentRepairMapper;


    @Test
    void contextLoads() {
        EquipmentRepairQueryDTO equipmentRepairQueryDTO = new EquipmentRepairQueryDTO();
        equipmentRepairQueryDTO.setAccount("440514");
        equipmentRepairQueryDTO.setPage(1);
        equipmentRepairQueryDTO.setPageSize(12);
        Page<EquipmentRepairPageQueryVO> equipmentRepairs = equipmentRepairMapper.pageQueryTeacher(equipmentRepairQueryDTO);

        System.out.println(equipmentRepairs.getResult());

    }

    }
