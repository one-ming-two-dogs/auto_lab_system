package com.auto.pojo.vo;

import com.auto.result.PageResult;
import lombok.Data;

import java.io.Serializable;

/**
 * @author LoungeXi
 * @description 学生登陆返回VO
 */
@Data
public class StudentLoginVO extends UserLoginVO implements Serializable {

    // 专业
    private String major;

    // 班级
    private String classNumber;

}
