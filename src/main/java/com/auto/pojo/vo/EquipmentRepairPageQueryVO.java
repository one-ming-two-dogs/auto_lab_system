package com.auto.pojo.vo;


import com.auto.pojo.entity.Laboratory;
import com.auto.pojo.entity.Teacher;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author LoungeXi
 * @description 设备报修返回VO
 */
@Data
public class EquipmentRepairPageQueryVO {

    private Integer id;

    // 申请教师
    private Teacher teacher;

    //报修实验室id
    private Integer labId;

    // 报修实验室
    private Laboratory laboratory;

    // 故障描述
    private String issueDescription;

    // 报修日期
    private LocalDateTime repairDate;

    // 报修状态(已维修/未维修/维修中)
    private String repairStatus;

    // 维修情况说明
    private String repairNotes;
}
