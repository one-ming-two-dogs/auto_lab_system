package com.auto.pojo.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * @author LoungeXi
 * @description 查询学生返回VO
 */
@Data
public class StudentPageQueryVO implements Serializable {
    // 账号
    private String account;

    private String name;

    // 专业
    private String major;

    // 班级
    private String classNumber;
}
