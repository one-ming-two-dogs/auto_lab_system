package com.auto.pojo.vo;

import com.auto.pojo.entity.Semester;
import com.auto.pojo.entity.Teacher;
import lombok.Data;

/**
 * @author LoungeXi
 * @description 课程申请返回VO
 */
@Data
public class ClassApplicationPageQueryVO {

    // 申请id
    private Integer id;

    // 上课学期
    private Semester semester;

    // 申请教师姓名
    private String teacherName;

    // 申请教师账号
    private String teacherAccount;

    // 申请教师
    private Teacher teacher;

    // 课程名称
    private String courseName;

    // 实验室类型
    private String labType;

    // 学生班级
    private String studentClass;

    // 学生人数
    private Integer studentCount;

    // 开始周
    private Integer startWeek;

    // 结束周
    private Integer endWeek;

    // 星期几
    private Integer dayOfWeek;

    // 节次
    private String timeSlot;

    // 申请状态
    private String applicationStatus;

}
