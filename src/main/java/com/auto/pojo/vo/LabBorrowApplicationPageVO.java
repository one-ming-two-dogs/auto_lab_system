package com.auto.pojo.vo;

import com.auto.pojo.entity.Laboratory;
import com.auto.pojo.entity.Semester;
import com.auto.pojo.entity.Student;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author WisdomX
 * @description: 实验室借用申请返回VO
 */
@Data
public class LabBorrowApplicationPageVO {

    //申请Id
    private Integer id;
    // 学生账号
    private String studentAccount;
    // 学生
    private Student student;
    // 实验室Id
    private Integer labId;
    // 实验室
    private Laboratory laboratory;
    // 学期Id
    private Integer semesterId;
    // 学期
    private Semester semester;
    // 申请周次
    private Integer weekNumber;
    // 申请周几
    private Integer dayOfWeek;
    // 申请节次
    private String timeSlot;
    // 申请原因
    private String reason;
    // 申请状态
    private String applicationStatus;
    // 申请时间
    private LocalDateTime applicationDate;
}
