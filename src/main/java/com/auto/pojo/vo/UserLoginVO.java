package com.auto.pojo.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * @author LoungeXi
 * @description 用户登录返回VO
 */
@Data
public class UserLoginVO implements Serializable {

    private String account;

    private String name;

}
