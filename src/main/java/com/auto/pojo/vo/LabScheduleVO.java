package com.auto.pojo.vo;

import lombok.Data;

/**
 * @author WisdomX
 * @description: TODO
 */
@Data
public class LabScheduleVO {
    private String courseName;

    private String teacherName;

    private String className;

    private String week;

    private String timeSlot;

    private Integer dayOfWeek;

    private String labName;

    private String labNumber;
}
