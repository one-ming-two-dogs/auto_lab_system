package com.auto.pojo.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * @author LoungeXi
 * @description 学期信息返回VO
 */
@Data
public class SemesterVO implements Serializable {

    private Integer id;

    private String semester;

    private Integer startYear;

    private Integer endYear;

    private Integer quarter;
}
