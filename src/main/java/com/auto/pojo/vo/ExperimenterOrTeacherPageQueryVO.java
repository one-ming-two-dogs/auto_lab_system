package com.auto.pojo.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * @author LoungeXi
 * @description 查询实验员或教师返回VO
 */
@Data
public class ExperimenterOrTeacherPageQueryVO implements Serializable {
    // 账号
    private String account;

    private String name;

    // 职称
    private String jobTitle;

}
