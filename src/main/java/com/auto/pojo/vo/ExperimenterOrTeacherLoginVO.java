package com.auto.pojo.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * @author LoungeXi
 * @description 实验员或教师登录返回VO
 */
@Data
public class ExperimenterOrTeacherLoginVO extends UserLoginVO implements Serializable {

    //职称
    private String jobTitle;

}
