package com.auto.pojo.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * @author LoungeXi
 * @description 排课分页查询返回VO
 */
@Data
public class LabScheduleQueryVO implements Serializable {

    private Integer id;

    // 学期ID
    private Integer semesterId;

    // 教师账号
    private String teacherAccount;

    // 教师姓名
    private String teacherName;

    // 实验室编号
    private String labNumber;

    // 实验室名称
    private String labName;

    // 起始周
    private Integer startWeek;

    // 结束周
    private Integer endWeek;

    // 星期几
    private Integer dayOfWeek;

    // 节次
    private String timeSlot;

    // 课程名称
    private String courseName;

    // 学生班级
    private String studentClass;
}
