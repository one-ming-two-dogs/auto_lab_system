package com.auto.pojo.dto;

import io.swagger.models.auth.In;
import lombok.Data;

import java.io.Serializable;

/**
 * @author LoungeXi
 * @description 学生借用申请列表查询DTO
 */
@Data
public class StudentBorrowApplicationQueryDTO implements Serializable {
    //页码
    private Integer page;

    //每页记录数
    private Integer pageSize;

    //学生账号
    private String account;
}
