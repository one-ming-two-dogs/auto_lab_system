package com.auto.pojo.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @author LoungeXi
 * @description 学生分页查询请求DTO
 */
@Data
public class UserQueryDTO implements Serializable {

    //页码
    private int page;

    //每页记录数
    private int pageSize;

    //姓名
    private String name;

}
