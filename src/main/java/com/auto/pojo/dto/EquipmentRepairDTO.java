package com.auto.pojo.dto;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class EquipmentRepairDTO {

    // 报修id
    private Integer id;

    // 申请教师账号
    private String teacherAccount;

    // 报修实验室ID
    private Integer labId;

    // 故障描述
    private String issueDescription;

    // 报修状态(已维修/未维修/维修中)
    private String repairStatus;

    // 维修情况说明
    private String repairNotes;


}
