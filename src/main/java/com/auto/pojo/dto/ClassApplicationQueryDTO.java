package com.auto.pojo.dto;

import lombok.Data;

/**
 * @author LoungeXi
 * @description 课程申请表DTO
 */
@Data
public class ClassApplicationQueryDTO {

    //页码
    private int page;

    //每页记录数
    private int pageSize;

    // 教师账号
    private String account;
}
