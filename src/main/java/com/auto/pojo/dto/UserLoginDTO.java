package com.auto.pojo.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @author LoungeXi
 * @description 用户登录DTO
 */
@Data
public class UserLoginDTO implements Serializable {

    private String account;

    private String passWord;
}
