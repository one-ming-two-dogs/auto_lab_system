package com.auto.pojo.dto;

import lombok.Data;

@Data
public class ClassApplicationDTO {

    // 申请id
    private Integer id;

    // 学期ID
    private Integer semesterId;

    // 教师账号
    private String teacherAccount;

    // 教师姓名
    private String teacherName;

    // 课程名称
    private String courseName;

    // 实验室类别
    private String labType;

    // 学生班级
    private String studentClass;

    // 学生人数
    private Integer studentCount;

    // 起始周
    private Integer startWeek;

    // 结束周
    private Integer endWeek;

    // 星期几
    private Integer dayOFWeek;

    // 节次
    private String timeSlot;

}
