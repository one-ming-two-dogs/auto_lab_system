package com.auto.pojo.dto;

import lombok.Data;

/**
 * @author WisdomX
 * @description: 实验室申请DTO
 */
@Data
public class LabApplicationDTO {
    // 实验室申请ID
    private Integer id;

    private String teacherAccount;

    private String labType;

    private Integer studentCount;

    private Integer startWeek;

    private Integer endWeek;

    private Integer dayOfWeek;

    private String timeSlot;

    private String courseName;

    private String studentClass;
}
