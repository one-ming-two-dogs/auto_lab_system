package com.auto.pojo.dto;

import lombok.Data;

/**
 * @author WisdomX
 * @description: TODO
 */
@Data
public class LabBorrowDTO {
    // 申请周次
    private Integer weekNumber;
    // 申请周几
    private Integer dayOfWeek;
    // 申请节次
    private String timeSlot;
}
