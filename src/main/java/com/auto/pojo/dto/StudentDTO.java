package com.auto.pojo.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @author LoungeXi
 * @description 学生DTO
 */
@Data
public class StudentDTO implements Serializable {

    // 账号
    private String account;

    private String name;

    // 专业
    private String major;

    // 班级
    private String classNumber;

    private String passWord;
}
