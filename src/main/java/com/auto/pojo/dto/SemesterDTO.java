package com.auto.pojo.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @author LoungeXi
 * @description 学期DTO
 */
@Data
public class SemesterDTO implements Serializable {

    private  String currentSemester;

}
