package com.auto.pojo.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @author LoungeXi
 * @description 实验员或教师DTO
 */
@Data
public class ExperimenterOrTeacherDTO implements Serializable {
    // 账号
    private String account;

    private String name;

    // 职称
    private String jobTitle;

    private String passWord;
}
