package com.auto.pojo.dto;

import lombok.Data;

/**
 * @author WisdomX
 * @description: 实验室借用申请查询DTO
 */
@Data
public class LabBorrowApplicationQueryDTO {
    //页码
    private Integer page;

    //每页记录数
    private Integer pageSize;
}
