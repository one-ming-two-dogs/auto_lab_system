package com.auto.pojo.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @author LoungeXi
 * @description 实验室排课分页查询DTO
 */
@Data
public class LabScheduleQueryDTO implements Serializable {
    //页码
    private int page;

    //每页记录数
    private int pageSize;
}
