package com.auto.pojo.dto;

import lombok.Data;

@Data
public class EquipmentRepairQueryDTO {
    //页码
    private Integer page;

    //每页记录数
    private Integer pageSize;

    // 教师账号
    private String account;

}
