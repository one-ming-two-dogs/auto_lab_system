package com.auto.pojo.dto;

import com.auto.pojo.entity.Laboratory;
import com.auto.pojo.entity.Semester;
import com.auto.pojo.entity.Student;
import lombok.Data;
import org.apache.ibatis.annotations.Insert;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author LoungeXi
 * @description 借用申请DTO
 */
@Data
public class LabBorrowApplicationDTO implements Serializable {
    // 申请Id
    private Integer id;
    // 学生账号
    private String studentAccount;
    // 实验室Id
    private Integer labId;
    // 学期Id
    private Integer semesterId;
    // 申请周次
    private Integer weekNumber;
    // 申请周几
    private Integer dayOfWeek;
    // 申请节次
    private String timeSlot;
    // 申请原因
    private String reason;
    // 申请状态
    private String applicationStatus;
}
