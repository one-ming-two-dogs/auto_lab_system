package com.auto.pojo.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author LoungeXi
 * @description 管理员实体
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Administrator implements Serializable {

    private Integer id;

    // 账号
    private String account;

    private String name;

    private String passWord;

}
