package com.auto.pojo.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author LoungeXi
 * @description 实验课申请登记表实体
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class LabClassApplication implements Serializable {

    private Integer id;

    // 学期ID
    private Integer semesterId;

    // 教师账号
    private String teacherAccount;

    //教师姓名
    private String teacherName;

    // 课程名称
    private String courseName;

    // 实验室类别
    private String labType;

    // 学生班级
    private String studentClass;

    // 学生人数
    private Integer studentCount;

    // 起始周
    private Integer startWeek;

    // 结束周
    private Integer endWeek;

    // 星期几
    private Integer dayOFWeek;

    // 节次
    private String timeSlot;

    // 申请状态(已排课/未排课)
    private String applicationStatus;

}
