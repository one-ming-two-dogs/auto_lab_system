package com.auto.pojo.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author LoungeXi
 * @description 实验员实体
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Experimenter implements Serializable {

    private Integer id;

    // 账号
    private String account;

    private String name;

    // 职称
    private String jobTitle;

    private String passWord;

}
