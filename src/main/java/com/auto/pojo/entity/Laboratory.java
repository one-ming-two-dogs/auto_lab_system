package com.auto.pojo.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author LoungeXi
 * @description 实验室实体
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Laboratory implements Serializable {

    private Integer id;

    // 实验员账号
    private String experimenterAccount;

    // 实验室编号
    private String labNumber;

    // 实验室名称
    private String labName;

    // 实验室类别
    private String labType;

    // 设备数
    private Integer equipmentCount;

}
