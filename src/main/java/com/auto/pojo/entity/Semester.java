package com.auto.pojo.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author LoungeXi
 * @description 学期
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Semester implements Serializable {

    private Integer id;

    // 开始年份
    private Integer startYear;

    // 结束年份
    private Integer endYear;

    // 季度 1:秋季 2:春季
    private Integer quarter;

    //是否当前学期
    private Boolean isCurrentSemester;

}
