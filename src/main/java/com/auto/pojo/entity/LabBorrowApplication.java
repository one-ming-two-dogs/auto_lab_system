package com.auto.pojo.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author LoungeXi
 * @description 实验室借用申请表实体
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class LabBorrowApplication implements Serializable {

    private Integer id;

    // 学生账号
    private String studentAccount;

    // 实验室ID
    private Integer labId;

    // 学期ID
    private Integer semesterId;

    // 申请周次
    private Integer weekNumber;

    // 申请星期几
    private Integer dayOfWeek;

    // 申请节次
    private String timeSlot;

    // 申请原因
    private String reason;

    // 申请状态(未审核/通过/驳回/使用完毕)
    private String applicationStatus;

    // 填报日期
    private LocalDateTime applicationDate;

}
