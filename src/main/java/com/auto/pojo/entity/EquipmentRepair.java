package com.auto.pojo.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author LoungeXi
 * @description 设备报修实体
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class EquipmentRepair implements Serializable {

    private Integer id;

    // 申请教师账号
    private String teacherAccount;

    // 报修所属实验员账号
    private String experimenterAccount;

    // 报修实验室ID
    private Integer labId;

    // 故障描述
    private String issueDescription;

    // 报修日期
    private LocalDateTime repairDate;

    // 报修状态(已维修/未维修/维修中)
    private String repairStatus;

    // 维修情况说明
    private String repairNotes;

}
