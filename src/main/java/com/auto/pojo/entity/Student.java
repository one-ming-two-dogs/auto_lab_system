package com.auto.pojo.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author LoungeXi
 * @description 学生实体
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Student implements Serializable {

    private Integer id;

    // 账号
    private String account;

    private String name;

    // 专业
    private String major;

    // 班级
    private String classNumber;

    private String passWord;

}

