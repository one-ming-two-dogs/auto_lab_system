package com.auto.aspect;

import com.auto.context.SemesterContext;
import com.auto.mapper.SemesterMapper;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author LoungeXi
 * @description 将当前学期存入ThreadLocal钟的切面
 */
@Slf4j
@Component
@Aspect
public class SetCurrentSemesterAspect {

    @Resource
    private SemesterMapper semesterMapper;

    @Pointcut("execution(* com.auto.controller.*.login(..))")
    public void setCurrentSemesterAspectPointCut() {
    }

    @After("setCurrentSemesterAspectPointCut()")
    public void setCurrentSemester() {
        Integer id = SemesterContext.CURRENT_SEMESTER_ID;
        if (id == null) {
            log.info("向全局变量中存入当前学期的ID...");
            Integer currentId = semesterMapper.selectCurrentSemester().getId();
            SemesterContext.CURRENT_SEMESTER_ID = currentId;
            log.info("当前学期ID为:{}", currentId);
        } else {
            log.info("学期已经被设置过!");
            log.info("当前学期ID为:{}", id);
        }
    }
}
