package com.auto.constant;

/**
 * @author LoungeXi
 * @description 信息提示常量类
 */
public class MessageConstant {

    public static final String PASSWORD_ERROR = "密码错误";
    public static final String ACCOUNT_NOT_FOUND = "账号不存在";
    public static final String ACCOUNT_LOCKED = "账号被锁定";
    public static final String UNKNOWN_ERROR = "未知错误";
    public static final String ALREADY_EXISTS = "已存在";
    public static final String USER_TYPE_ERR = "导入用户类型错误";
    public static final String LOGIN_FAILED = "登录失败";
    public static final String FILE_NOT_FOUND = "未选择文件,请选择xls或者xlsx文件";
    public static final String FILE_FORMAT_ERR = "文件格式错误,请选择xls或者xlsx文件";
    public static final String IMPORT_SUCCESS = "导入数据成功";
    public static final String PASSWORD_EDIT_FAILED = "密码修改失败";
    public static final String APPLICATION_STATUS_ERROR = "申请状态错误";
    public static final String DEFAULT_PASSWORD = "123456";
}
