package com.auto.context;

/**
 * @author LoungeXi
 * @description 全局信息存储中间类
 */
public class SemesterContext {
    // 存储当前学期ID
    public static Integer CURRENT_SEMESTER_ID;
}
