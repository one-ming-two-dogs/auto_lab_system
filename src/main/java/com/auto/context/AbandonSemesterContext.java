package com.auto.context;

/**
 * @author LoungeXi
 * @description 当前学期缓存存储区
 */
public class AbandonSemesterContext {
    public static ThreadLocal<Integer> threadLocal = new ThreadLocal<>();

    public static void setCurrentSemester(Integer id) {
        threadLocal.set(id);
    }

    public static Integer getCurrentSemester() {
        return threadLocal.get();
    }

    public static void removeCurrentSemester() {
        threadLocal.remove();
    }
}
