package com.auto.controller;

import com.auto.pojo.dto.ClassApplicationDTO;
import com.auto.pojo.dto.ClassApplicationQueryDTO;
import com.auto.pojo.dto.EquipmentRepairDTO;
import com.auto.pojo.dto.EquipmentRepairQueryDTO;
import com.auto.result.PageResult;
import com.auto.result.Result;
import com.auto.service.EquipmentRepairService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author LoungeXi
 * @description 设备报修控制器
 */
@RestController
@Slf4j
@Api(tags = "设备报修相关接口")
@RequestMapping("/auto/equipmentRepair")
public class EquipmentRepairController {
    @Resource
    private EquipmentRepairService equipmentRepairService;

    @ApiOperation(value = "教师设备报修分页查询")
    @RequestMapping(value = "/pageTeacher", method = RequestMethod.GET)
    public Result teacherEquipmentRepairPageQuery(EquipmentRepairQueryDTO equipmentRepairQueryDTO) {
        log.info("教师设备报修分页查询:{}", equipmentRepairQueryDTO);
        PageResult pageResult = equipmentRepairService.teacherEquipmentRepairPageQuery(equipmentRepairQueryDTO);
        return Result.success(pageResult);
    }

    @ApiOperation(value = "新增教师设备报修")
    @RequestMapping(value = "/saveTeacher", method = RequestMethod.POST)
    public Result save(@RequestBody EquipmentRepairDTO equipmentRepairDTO) {
        log.info("新增教师课程申请:{}", equipmentRepairDTO);
        equipmentRepairService.saveTeacher(equipmentRepairDTO);
        return Result.success();
    }


    @ApiOperation(value = "实验员设备报修分页查询")
    @RequestMapping(value = "/pageExperimenter", method = RequestMethod.GET)
    public Result experimenterEquipmentRepairPageQuery(EquipmentRepairQueryDTO equipmentRepairQueryDTO) {
        log.info("实验员设备报修分页查询:{}", equipmentRepairQueryDTO);
        PageResult pageResult = equipmentRepairService.experimenterEquipmentRepairPageQuery(equipmentRepairQueryDTO);
        return Result.success(pageResult);
    }

    @ApiOperation(value = "设备报修情况更新")
    @RequestMapping(value = "/update", method = RequestMethod.PUT)
    public Result update(@RequestBody EquipmentRepairDTO equipmentRepairDTO) {
        log.info("设备报修情况更新:{}", equipmentRepairDTO);
        equipmentRepairService.update(equipmentRepairDTO);
        return Result.success();
    }

}
