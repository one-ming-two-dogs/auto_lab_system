package com.auto.controller;

import com.auto.pojo.dto.*;
import com.auto.pojo.entity.Laboratory;
import com.auto.result.PageResult;
import com.auto.result.Result;
import com.auto.service.LabBorrowApplicationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author LoungeXi
 * @description 实验室借用申请控制器
 */

@RestController
@Slf4j
@Api(tags = "实验室借用相关接口")
@RequestMapping("/auto/labBorrowApplication")
public class LabBorrowApplicationController {

    @Resource
    private LabBorrowApplicationService labBorrowApplicationService;

    @ApiOperation(value = "管理员查询借用申请")
    @RequestMapping(value = "/page", method = RequestMethod.GET)
    public Result pageQuery(LabBorrowApplicationQueryDTO labBorrowApplicationQueryDTO) {
        PageResult pageResult = labBorrowApplicationService.pageQuery(labBorrowApplicationQueryDTO);
        return Result.success(pageResult);
    }

    @ApiOperation(value = "学生查询借用申请")
    @RequestMapping(value = "/pageStudent", method = RequestMethod.GET)
    public Result pageQueryStudent(StudentBorrowApplicationQueryDTO studentBorrowApplicationQueryDTO) {
        PageResult pageResult = labBorrowApplicationService.pageQueryStudent(studentBorrowApplicationQueryDTO);
        return Result.success(pageResult);
    }

    @ApiOperation(value = "更新借用申请")
    @RequestMapping(value = "/update", method = RequestMethod.PUT)
    public Result update(@RequestBody LabBorrowApplicationDTO labBorrowApplicationDTO) {
        log.info("更新借用申请:{}", labBorrowApplicationDTO);
        labBorrowApplicationService.update(labBorrowApplicationDTO);
        return Result.success();
    }

    @ApiOperation(value = "新增借用申请")
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public Result save(@RequestBody LabBorrowApplicationDTO labBorrowApplicationDTO) {
        log.info("新增借用申请:{}", labBorrowApplicationDTO);
        labBorrowApplicationService.save(labBorrowApplicationDTO);
        return Result.success();
    }

    @ApiOperation(value = "查询可用实验室")
    @RequestMapping(value = "/get", method = RequestMethod.POST)
    public Result getAvailableLabs(@RequestBody LabBorrowDTO labBorrowDTO) {
        List<Laboratory> result = labBorrowApplicationService.getAvailableLabs(labBorrowDTO);
        return Result.success(result);
    }
}
