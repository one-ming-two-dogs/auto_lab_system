package com.auto.controller;

import com.auto.pojo.dto.ExperimenterOrTeacherDTO;
import com.auto.pojo.dto.UserLoginDTO;
import com.auto.pojo.dto.UserQueryDTO;
import com.auto.pojo.entity.Administrator;
import com.auto.pojo.entity.Experimenter;
import com.auto.pojo.vo.ExperimenterOrTeacherLoginVO;
import com.auto.pojo.vo.UserLoginVO;
import com.auto.result.PageResult;
import com.auto.result.Result;
import com.auto.service.ExperimenterService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author LoungeXi
 * @description 实验员控制器
 */
@RestController
@Slf4j
@Api(tags = "实验员相关接口")
@RequestMapping("/auto/experimenter")
public class ExperimenterController {
    @Resource
    private ExperimenterService experimenterService;

    @ApiOperation(value = "实验员分页模糊查询")
    @RequestMapping(value = "/page", method = RequestMethod.GET)
    public Result experimenterPageQuery(UserQueryDTO userQueryDTO) {
        log.info("实验员分页查询:{}", userQueryDTO);
        PageResult pageResult = experimenterService.pageQuery(userQueryDTO);
        return Result.success(pageResult);
    }

    @ApiOperation(value = "新增实验员")
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public Result save(@RequestBody ExperimenterOrTeacherDTO experimenterOrTeacherDTO) {
        log.info("新增实验员:{}", experimenterOrTeacherDTO);
        experimenterService.save(experimenterOrTeacherDTO);
        return Result.success();
    }

    @ApiOperation(value = "删除实验员")
    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    public Result deleteByAccount(String account) {
        log.info("删除实验员:{}", account);
        experimenterService.deleteByAccount(account);
        return Result.success();
    }

    @ApiOperation(value = "修改实验员信息")
    @RequestMapping(value = "/update", method = RequestMethod.PUT)
    public Result update(@RequestBody ExperimenterOrTeacherDTO experimenterOrTeacherDTO) {
        log.info("修改实验员信息:{}", experimenterOrTeacherDTO);
        experimenterService.update(experimenterOrTeacherDTO);
        return Result.success();
    }

    @ApiOperation(value = "实验员登录接口")
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public Result login(@RequestBody UserLoginDTO userLoginDTO) {
        log.info("实验员登录信息:{}", userLoginDTO);
        Experimenter experimenter = experimenterService.login(userLoginDTO);
        ExperimenterOrTeacherLoginVO experimenterOrTeacherLoginVO = new ExperimenterOrTeacherLoginVO();
        experimenterOrTeacherLoginVO.setAccount(experimenter.getAccount());
        experimenterOrTeacherLoginVO.setName(experimenter.getName());
        experimenterOrTeacherLoginVO.setJobTitle(experimenter.getJobTitle());
        return Result.success(experimenterOrTeacherLoginVO);
    }
}
