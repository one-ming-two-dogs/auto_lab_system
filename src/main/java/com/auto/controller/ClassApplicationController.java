package com.auto.controller;

import com.auto.pojo.dto.ClassApplicationDTO;
import com.auto.pojo.dto.ClassApplicationQueryDTO;
import com.auto.pojo.dto.StudentDTO;
import com.auto.result.PageResult;
import com.auto.result.Result;
import com.auto.service.ClassApplicationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author LoungeXi
 * @description 课程申请控制器
 */
@RestController
@Slf4j
@Api(tags = "课程申请相关接口")
@RequestMapping("/auto/classApplication")
public class ClassApplicationController {

    @Resource
    private ClassApplicationService classApplicationService;

    @ApiOperation(value = "课程申请分页查询")
    @RequestMapping(value = "/page", method = RequestMethod.GET)
    public Result classApplicationPageQuery(ClassApplicationQueryDTO classApplicationQueryDTO) {
        log.info("课程申请分页查询:{}", classApplicationQueryDTO);
        PageResult pageResult = classApplicationService.pageQuery(classApplicationQueryDTO);
        return Result.success(pageResult);
    }

    @ApiOperation(value = "新增教师课程申请")
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public Result save(@RequestBody ClassApplicationDTO classApplicationDTO) {
        log.info("新增教师课程申请:{}", classApplicationDTO);
        classApplicationService.save(classApplicationDTO);
        return Result.success();
    }

    @ApiOperation(value = "修改教师课程申请")
    @RequestMapping(value = "/update", method = RequestMethod.PUT)
    public Result update(@RequestBody ClassApplicationDTO classApplicationDTO) {
        log.info("修改教师课程申请:{}", classApplicationDTO);
        classApplicationService.update(classApplicationDTO);
        return Result.success();
    }

    @ApiOperation(value = "删除教师课程申请")
    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    public Result deleteById(Integer id) {
        log.info("删除教师课程申请:{}", id);
        classApplicationService.deleteById(id);
        return Result.success();
    }
}
