package com.auto.controller;

import com.auto.constant.MessageConstant;
import com.auto.pojo.dto.EquipmentRepairQueryDTO;
import com.auto.result.PageResult;
import com.auto.result.Result;
import com.auto.service.CommonService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;

/**
 * @author LoungeXi
 * @description 通用接口控制器
 */

@RestController
@Slf4j
@Api(tags = "通用接口")
@RequestMapping("/auto/common")
public class CommonController {
    @Resource
    private CommonService commonService;

    @ApiOperation(value = "批量导入用户")
    @RequestMapping(value = "/bulkImport", method = RequestMethod.POST)
    public Result<String> teacherEquipmentRepairPageQuery(@RequestPart("file") @RequestParam(required = false) MultipartFile file) {
        log.info("文件上传:{}", file);
       String resultString = commonService.bulkImport(file);
        return Result.success(resultString);
    }
}
