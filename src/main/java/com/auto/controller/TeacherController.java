package com.auto.controller;

import com.auto.pojo.dto.ExperimenterOrTeacherDTO;
import com.auto.pojo.dto.UserLoginDTO;
import com.auto.pojo.dto.UserQueryDTO;
import com.auto.pojo.entity.Experimenter;
import com.auto.pojo.entity.Teacher;
import com.auto.pojo.vo.ExperimenterOrTeacherLoginVO;
import com.auto.result.PageResult;
import com.auto.result.Result;
import com.auto.service.TeacherService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author LoungeXi
 * @description 教师控制器
 */
@RestController
@Slf4j
@Api(tags = "教师相关接口")
@RequestMapping("/auto/teacher")
public class TeacherController {
    @Resource
    private TeacherService teacherService;

    @ApiOperation(value = "教师分页模糊查询")
    @RequestMapping(value = "/page", method = RequestMethod.GET)
    public Result teacherPageQuery(UserQueryDTO userQueryDTO) {
        log.info("教师分页查询:{}", userQueryDTO);
        PageResult pageResult = teacherService.pageQuery(userQueryDTO);
        return Result.success(pageResult);
    }

    @ApiOperation(value = "新增教师")
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public Result save(@RequestBody ExperimenterOrTeacherDTO experimenterOrTeacherDTO) {
        log.info("新增教师:{}", experimenterOrTeacherDTO);
        teacherService.save(experimenterOrTeacherDTO);
        return Result.success();
    }

    @ApiOperation(value = "删除教师")
    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    public Result deleteByAccount(String account) {
        log.info("删除教师:{}", account);
        teacherService.deleteByAccount(account);
        return Result.success();
    }

    @ApiOperation(value = "修改教师信息")
    @RequestMapping(value = "/update", method = RequestMethod.PUT)
    public Result update(@RequestBody ExperimenterOrTeacherDTO experimenterOrTeacherDTO) {
        log.info("修改教师信息:{}", experimenterOrTeacherDTO);
        teacherService.update(experimenterOrTeacherDTO);
        return Result.success();
    }

    @ApiOperation(value = "教师登录接口")
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public Result login(@RequestBody UserLoginDTO userLoginDTO) {
        log.info("教师登录信息:{}", userLoginDTO);
        Teacher teacher = teacherService.login(userLoginDTO);
        ExperimenterOrTeacherLoginVO experimenterOrTeacherLoginVO = new ExperimenterOrTeacherLoginVO();
        experimenterOrTeacherLoginVO.setAccount(teacher.getAccount());
        experimenterOrTeacherLoginVO.setName(teacher.getName());
        experimenterOrTeacherLoginVO.setJobTitle(teacher.getJobTitle());
        return Result.success(experimenterOrTeacherLoginVO);
    }
}
