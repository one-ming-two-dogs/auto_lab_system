package com.auto.controller;

import com.auto.pojo.dto.UserLoginDTO;
import com.auto.pojo.dto.UserQueryDTO;
import com.auto.pojo.entity.Administrator;
import com.auto.pojo.entity.Experimenter;
import com.auto.pojo.vo.UserLoginVO;
import com.auto.result.PageResult;
import com.auto.result.Result;
import com.auto.service.AdministratorService;
import com.auto.service.impl.AdministratorServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author LoungeXi
 * @description 系统管理员控制器
 */
@RestController
@Slf4j
@Api(tags = "系统管理员相关接口")
@RequestMapping("/auto/administrator")
public class AdministratorController {
    @Resource
    private AdministratorService administratorService;

    @ApiOperation(value = "系统管理员登录接口")
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public Result login(@RequestBody UserLoginDTO userLoginDTO) {
        log.info("系统管理员登录信息:{}", userLoginDTO);
        Administrator administrator = administratorService.login(userLoginDTO);
        UserLoginVO userLoginVO = new UserLoginVO();
        userLoginVO.setAccount(administrator.getAccount());
        userLoginVO.setName(administrator.getName());
        return Result.success(userLoginVO);
    }

}
