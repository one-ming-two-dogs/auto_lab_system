package com.auto.controller;

import com.auto.pojo.dto.StudentDTO;
import com.auto.pojo.dto.UserLoginDTO;
import com.auto.pojo.dto.UserQueryDTO;
import com.auto.pojo.entity.Student;
import com.auto.pojo.entity.Teacher;
import com.auto.pojo.vo.ExperimenterOrTeacherLoginVO;
import com.auto.pojo.vo.StudentLoginVO;
import com.auto.result.PageResult;
import com.auto.result.Result;
import com.auto.service.StudentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author LoungeXi
 * @description 学生控制器
 */
@RestController
@Slf4j
@Api(tags = "学生相关接口")
@RequestMapping("/auto/student")
public class StudentController {
    @Resource
    private StudentService studentService;


    @ApiOperation(value = "学生分页模糊查询")
    @RequestMapping(value = "/page", method = RequestMethod.GET)
    public Result studentPageQuery(UserQueryDTO userQueryDTO) {
        log.info("学生分页查询:{}", userQueryDTO);
        PageResult pageResult = studentService.pageQuery(userQueryDTO);
        return Result.success(pageResult);
    }

    @ApiOperation(value = "新增学生")
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public Result save(@RequestBody StudentDTO studentDTO) {
        log.info("新增学生:{}", studentDTO);
        studentService.save(studentDTO);
        return Result.success();
    }

    @ApiOperation(value = "删除学生")
    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    public Result deleteByAccount(String account) {
        log.info("删除学生:{}", account);
        studentService.deleteByAccount(account);
        return Result.success();
    }

    @ApiOperation(value = "修改学生信息")
    @RequestMapping(value = "/update", method = RequestMethod.PUT)
    public Result update(@RequestBody StudentDTO studentDTO) {
        log.info("修改学生信息:{}", studentDTO);
        studentService.update(studentDTO);
        return Result.success();
    }

    @ApiOperation(value = "学生登录接口")
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public Result login(@RequestBody UserLoginDTO userLoginDTO) {
        log.info("学生登录信息:{}", userLoginDTO);
        Student student = studentService.login(userLoginDTO);
        StudentLoginVO studentLoginVO = new StudentLoginVO();
        studentLoginVO.setAccount(student.getAccount());
        studentLoginVO.setName(student.getName());
        studentLoginVO.setMajor(student.getMajor());
        studentLoginVO.setClassNumber(student.getClassNumber());
        return Result.success(studentLoginVO);
    }
}
