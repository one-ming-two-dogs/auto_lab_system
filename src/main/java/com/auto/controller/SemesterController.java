package com.auto.controller;

import com.auto.pojo.dto.SemesterDTO;
import com.auto.pojo.entity.Semester;
import com.auto.pojo.vo.SemesterVO;
import com.auto.result.Result;
import com.auto.service.SemesterService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author LoungeXi
 * @description 学期控制器
 */
@RestController
@Slf4j
@Api(tags = "学期相关接口")
@RequestMapping("/auto/semester")
public class SemesterController {
    @Resource
    private SemesterService semesterService;

    @ApiOperation(value = "查询所有学期")
    @RequestMapping(value = "/select", method = RequestMethod.GET)
    public Result selectAllSemester() {
        List<SemesterVO> semestersList = semesterService.selectAllSemester();
        return Result.success(semestersList);
    }

    @ApiOperation(value = "设置当前学期")
    @RequestMapping(value = "/setCurrentSemester", method = RequestMethod.PUT)
    public Result setCurrentSemester(@RequestBody SemesterDTO semesterDTO) {
        semesterService.setCurrentSemester(semesterDTO);
        return Result.success();
    }

    @ApiOperation(value = "查询当前学期")
    @RequestMapping(value = "/selectCurrentSemester", method = RequestMethod.GET)
    public Result selectCurrentSemester() {
        Semester semester = semesterService.selectCurrentSemester();
        SemesterVO semesterVO = new SemesterVO();
        semesterVO.setId(semester.getId());
        semesterVO.setStartYear(semester.getStartYear());
        semesterVO.setEndYear(semester.getEndYear());
        semesterVO.setQuarter(semester.getQuarter());
        semesterVO.setSemester(semesterVO.getStartYear() + "-" + semesterVO.getEndYear() + "-" + semesterVO.getQuarter());
        return Result.success(semesterVO);
    }
}
