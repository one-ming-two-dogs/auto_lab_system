package com.auto.controller;

import com.auto.pojo.dto.LabApplicationDTO;
import com.auto.pojo.dto.LabScheduleQueryDTO;
import com.auto.pojo.dto.UserQueryDTO;
import com.auto.pojo.vo.LabScheduleVO;
import com.auto.result.PageResult;
import com.auto.result.Result;
import com.auto.service.LabScheduleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author WisdomX
 * @description: 实验室排课控制器
 */
@RestController
@Slf4j
@Api(tags = "实验室排课相关接口")
@RequestMapping("/auto/labSchedule")
public class LabScheduleController {
    @Resource
    private LabScheduleService labScheduleService;

    @ApiOperation("给实验室排课接口")
    @RequestMapping(value = "/arrange", method = RequestMethod.POST)
    public Result arrange(@RequestBody LabApplicationDTO labApplicationDTO) {
        Boolean ok = labScheduleService.arrangeLabs(labApplicationDTO);
        if (!ok) {
            return Result.error("排课失败!");
        }
        return Result.success("排课成功!");
    }

    @ApiOperation("获取实验室排课表")
    @RequestMapping(value = "/get", method = RequestMethod.GET)
    public Result getLabSchedule() {
        List<LabScheduleVO> labScheduleList = labScheduleService.getLabSchedule();
        return Result.success(labScheduleList);
    }

    @ApiOperation(value = "实验室排课分页查询")
    @RequestMapping(value = "/page", method = RequestMethod.GET)
    public Result teacherPageQuery(LabScheduleQueryDTO labScheduleQueryDTO) {
        log.info("实验室排课分页查询:{}", labScheduleQueryDTO);
        PageResult pageResult = labScheduleService.pageQuery(labScheduleQueryDTO);
        return Result.success(pageResult);
    }
}
