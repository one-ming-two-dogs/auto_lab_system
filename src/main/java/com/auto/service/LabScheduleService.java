package com.auto.service;

import com.auto.pojo.dto.LabApplicationDTO;
import com.auto.pojo.dto.LabScheduleQueryDTO;
import com.auto.pojo.entity.LabSchedule;
import com.auto.pojo.vo.LabScheduleVO;
import com.auto.result.PageResult;

import java.util.List;

public interface LabScheduleService {
    Boolean arrangeLabs(LabApplicationDTO labApplicationDTO);

    List<LabScheduleVO> getLabSchedule();

    PageResult pageQuery(LabScheduleQueryDTO labScheduleQueryDTO);
}
