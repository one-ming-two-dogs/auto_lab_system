package com.auto.service;


import com.auto.pojo.dto.ClassApplicationDTO;
import com.auto.pojo.dto.ClassApplicationQueryDTO;
import com.auto.result.PageResult;

/**
 * @author: LoungeXi
 * @date: 2024/4/16 11:03
 * @description: 课程申请接口
 */
public interface ClassApplicationService {
    PageResult pageQuery(ClassApplicationQueryDTO classApplicationQueryDTO);

    void save(ClassApplicationDTO classApplicationDTO);

    void update(ClassApplicationDTO classApplicationDTO);

    void deleteById(Integer id);
}
