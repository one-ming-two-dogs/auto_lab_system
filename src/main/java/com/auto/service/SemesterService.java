package com.auto.service;

import com.auto.pojo.dto.SemesterDTO;
import com.auto.pojo.entity.Semester;
import com.auto.pojo.vo.SemesterVO;

import java.util.List;

/**
 * @author: LoungeXi
 * @date: 2024/4/13 22:16
 * @description: 学期接口
 */
public interface SemesterService {
    List<SemesterVO> selectAllSemester();

    void setCurrentSemester(SemesterDTO semesterDTO);

    Semester selectCurrentSemester();
}
