package com.auto.service;

import org.springframework.web.multipart.MultipartFile;

/**
 * @author: LoungeXi
 * @date: 2024/4/19 11:03
 * @description: 通用接口
 */
public interface CommonService {
    String bulkImport(MultipartFile file);
}
