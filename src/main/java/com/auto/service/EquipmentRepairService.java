package com.auto.service;

import com.auto.pojo.dto.EquipmentRepairDTO;
import com.auto.pojo.dto.EquipmentRepairQueryDTO;
import com.auto.result.PageResult;

public interface EquipmentRepairService {
    PageResult teacherEquipmentRepairPageQuery(EquipmentRepairQueryDTO equipmentRepairQueryDTO);

    void saveTeacher(EquipmentRepairDTO equipmentRepairDTO);

    PageResult experimenterEquipmentRepairPageQuery(EquipmentRepairQueryDTO equipmentRepairQueryDTO);

    void update(EquipmentRepairDTO equipmentRepairDTO);
}
