package com.auto.service;

import com.auto.pojo.dto.ExperimenterOrTeacherDTO;
import com.auto.pojo.dto.UserLoginDTO;
import com.auto.pojo.dto.UserQueryDTO;
import com.auto.pojo.entity.Experimenter;
import com.auto.result.PageResult;

/**
 * @author: LoungeXi
 * @date: 2024/4/13 17:21
 * @description: 实验员接口
 */
public interface ExperimenterService {
    PageResult pageQuery(UserQueryDTO userQueryDTO);

    void save(ExperimenterOrTeacherDTO experimenterOrTeacherDTO);

    void deleteByAccount(String account);

    void update(ExperimenterOrTeacherDTO experimenterOrTeacherDTO);

    Experimenter login(UserLoginDTO userLoginDTO);
}
