package com.auto.service;

import com.auto.pojo.dto.UserLoginDTO;
import com.auto.pojo.entity.Administrator;
import org.springframework.stereotype.Service;

/**
 * @author: LoungeXi
 * @date: 2024/4/12 21:03
 * @description: 系统管理员接口
 */
public interface AdministratorService {


    Administrator login(UserLoginDTO userLoginDTO);
}
