package com.auto.service;

import com.auto.pojo.dto.LabBorrowApplicationDTO;
import com.auto.pojo.dto.LabBorrowDTO;
import com.auto.pojo.dto.StudentBorrowApplicationQueryDTO;
import com.auto.pojo.entity.LabBorrowApplication;
import com.auto.pojo.entity.Laboratory;

import java.util.List;
import com.auto.pojo.dto.LabBorrowApplicationQueryDTO;
import com.auto.result.PageResult;

public interface LabBorrowApplicationService {
    List<Laboratory> getAvailableLabs(LabBorrowDTO labBorrowDTO);
    PageResult pageQuery(LabBorrowApplicationQueryDTO labBorrowApplicationQueryDTO);

    void update(LabBorrowApplicationDTO labBorrowApplicationDTO);

    void save(LabBorrowApplicationDTO labBorrowApplicationDTO);

    PageResult pageQueryStudent(StudentBorrowApplicationQueryDTO studentBorrowApplicationQueryDTO);
}
