package com.auto.service.impl;

import com.auto.constant.MessageConstant;
import com.auto.exception.AccountNotFoundException;
import com.auto.exception.PasswordErrorException;
import com.auto.mapper.TeacherMapper;
import com.auto.pojo.dto.ExperimenterOrTeacherDTO;
import com.auto.pojo.dto.UserLoginDTO;
import com.auto.pojo.dto.UserQueryDTO;
import com.auto.pojo.entity.Experimenter;
import com.auto.pojo.entity.Teacher;
import com.auto.pojo.vo.ExperimenterOrTeacherPageQueryVO;
import com.auto.result.PageResult;
import com.auto.service.TeacherService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author LoungeXi
 * @description 教师接口实现类
 */
@Service
public class TeacherServiceImpl implements TeacherService {
    @Resource
    private TeacherMapper teacherMapper;
    @Override
    public PageResult pageQuery(UserQueryDTO userQueryDTO) {
        PageHelper.startPage(userQueryDTO.getPage(), userQueryDTO.getPageSize());
        Page<ExperimenterOrTeacherPageQueryVO> page = teacherMapper.pageQuery(userQueryDTO);
        return new PageResult(page.getTotal(), page.getResult());
    }

    @Override
    public void save(ExperimenterOrTeacherDTO experimenterOrTeacherDTO) {
        Teacher teacher = new Teacher();
        BeanUtils.copyProperties(experimenterOrTeacherDTO,teacher);
        teacherMapper.insert(teacher);
    }

    @Override
    public void deleteByAccount(String account) {
        teacherMapper.deleteByAccount(account);
    }

    @Override
    public void update(ExperimenterOrTeacherDTO experimenterOrTeacherDTO) {
        Teacher teacher = new Teacher();
        BeanUtils.copyProperties(experimenterOrTeacherDTO,teacher);
        teacherMapper.update(teacher);
    }

    @Override
    public Teacher login(UserLoginDTO userLoginDTO) {
        String account = userLoginDTO.getAccount();

        Teacher teacher = teacherMapper.getTeacherByAccount(account);

        if (teacher == null) {
            throw new AccountNotFoundException(MessageConstant.ACCOUNT_NOT_FOUND);
        }

        String passWord = userLoginDTO.getPassWord();
        if(!teacher.getPassWord().equals(passWord)){
            throw new PasswordErrorException(MessageConstant.PASSWORD_ERROR);
        }

        return teacher;
    }
}
