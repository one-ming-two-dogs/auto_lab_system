package com.auto.service.impl;

import com.auto.constant.MessageConstant;
import com.auto.exception.AccountNotFoundException;
import com.auto.exception.PasswordErrorException;
import com.auto.mapper.ExperimenterMapper;
import com.auto.pojo.dto.ExperimenterOrTeacherDTO;
import com.auto.pojo.dto.UserLoginDTO;
import com.auto.pojo.dto.UserQueryDTO;
import com.auto.pojo.entity.Administrator;
import com.auto.pojo.entity.Experimenter;
import com.auto.pojo.vo.ExperimenterOrTeacherPageQueryVO;
import com.auto.result.PageResult;
import com.auto.service.ExperimenterService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author LoungeXi
 * @description 实验员接口实现类
 */
@Service
public class ExperimenterServiceImpl implements ExperimenterService {
    @Resource
    private ExperimenterMapper experimenterMapper;
    @Override
    public PageResult pageQuery(UserQueryDTO userQueryDTO) {
        PageHelper.startPage(userQueryDTO.getPage(), userQueryDTO.getPageSize());
        Page<ExperimenterOrTeacherPageQueryVO> page = experimenterMapper.pageQuery(userQueryDTO);
        return new PageResult(page.getTotal(), page.getResult());
    }

    @Override
    public void save(ExperimenterOrTeacherDTO experimenterOrTeacherDTO) {
        Experimenter experimenter = new Experimenter();
        BeanUtils.copyProperties(experimenterOrTeacherDTO,experimenter);
        experimenterMapper.insert(experimenter);
    }

    @Override
    public void deleteByAccount(String account) {
        experimenterMapper.deleteByAccount(account);
    }

    @Override
    public void update(ExperimenterOrTeacherDTO experimenterOrTeacherDTO) {
        Experimenter experimenter = new Experimenter();
        BeanUtils.copyProperties(experimenterOrTeacherDTO,experimenter);
        experimenterMapper.update(experimenter);
    }

    @Override
    public Experimenter login(UserLoginDTO userLoginDTO) {
        String account = userLoginDTO.getAccount();

        Experimenter experimenter = experimenterMapper.getExperimenterByAccount(account);

        if (experimenter == null) {
            throw new AccountNotFoundException(MessageConstant.ACCOUNT_NOT_FOUND);
        }

        String passWord = userLoginDTO.getPassWord();
        if(!experimenter.getPassWord().equals(passWord)){
            throw new PasswordErrorException(MessageConstant.PASSWORD_ERROR);
        }

        return experimenter;
    }
}
