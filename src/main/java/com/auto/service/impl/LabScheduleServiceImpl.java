package com.auto.service.impl;

import com.auto.context.SemesterContext;
import com.auto.mapper.*;
import com.auto.pojo.dto.ClassApplicationDTO;
import com.auto.pojo.dto.LabApplicationDTO;
import com.auto.pojo.dto.LabScheduleQueryDTO;
import com.auto.pojo.entity.LabClassApplication;
import com.auto.pojo.entity.LabSchedule;
import com.auto.pojo.entity.Laboratory;
import com.auto.pojo.vo.LabScheduleQueryVO;
import com.auto.pojo.vo.LabScheduleVO;
import com.auto.result.PageResult;
import com.auto.service.LabScheduleService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.omg.PortableServer.POA;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @author WisdomX
 * @description: 实验排课接口实现类
 */
@Service
public class LabScheduleServiceImpl implements LabScheduleService {
    @Resource
    private LaboratoryMapper laboratoryMapper;
    @Resource
    private LabScheduleMapper labScheduleMapper;
    @Resource
    private TeacherMapper teacherMapper;
    @Resource
    SemesterMapper semesterMapper;
    @Resource
    ClassApplicationMapper classApplicationMapper;

    @Transactional
    @Override
    public Boolean arrangeLabs(LabApplicationDTO labApplicationDTO) {
        String appliedTeacherAccount = labApplicationDTO.getTeacherAccount();
        String appliedTeacherName = teacherMapper.getTeacherByAccount(appliedTeacherAccount).getName();
        Integer appliedStudentCount = labApplicationDTO.getStudentCount();
        Integer appliedDayOfWeek = labApplicationDTO.getDayOfWeek();
        String appliedTimeSlot = labApplicationDTO.getTimeSlot();
        Integer appliedStartWeek = labApplicationDTO.getStartWeek();
        Integer appliedEndWeek = labApplicationDTO.getEndWeek();
        String appliedCourseName = labApplicationDTO.getCourseName();
        String appliedStudentClass = labApplicationDTO.getStudentClass();
        Integer semesterId = semesterMapper.selectCurrentSemester().getId();
        // 所需实验室列表
        List<Laboratory> labs = laboratoryMapper.listLaboratoriesByType(labApplicationDTO.getLabType());
        // 已安排实验室列表
        List<LabSchedule> labSchedules = labScheduleMapper.getAllLabSchedules();
        // 冲突实验室
        List<Laboratory> conflictedLabs = new ArrayList<>();
        // 找到实验室安排和所需实验室的交集
        for (Laboratory laboratory : labs) {
            for (LabSchedule labSchedule : labSchedules) {
                // 不是同一个实验室
                if (!labSchedule.getLabNumber().equals(laboratory.getLabNumber())) {
                    continue;
                }
                Integer scheduledDayOfWeek = labSchedule.getDayOfWeek();
                String scheduledTimeSlot = labSchedule.getTimeSlot();
                Integer scheduledStartWeek = labSchedule.getStartWeek();
                Integer scheduledEndWeek = labSchedule.getEndWeek();
                // 如果起始周和结束周都大于或小于所安排的周，说明不冲突，看下一个被安排的实验室
                if (appliedStartWeek < scheduledStartWeek && appliedEndWeek <= scheduledStartWeek ||
                        appliedStartWeek >= scheduledEndWeek && appliedEndWeek > scheduledEndWeek) {
                    continue;
                }
                // 如果周几不同，说明不冲突，看下一个被安排的实验室
                if (!scheduledDayOfWeek.equals(appliedDayOfWeek)) {
                    continue;
                }
                // 如果节次不同，说明不冲突，看下一个被安排的实验室
                if (!scheduledTimeSlot.equals(appliedTimeSlot)) {
                    continue;
                }
                // 剩下的情况就是冲突的情况，即当前实验室有冲突
                conflictedLabs.add(laboratory);
                break;
            }
        }
        // 取所需实验室和冲突实验室的差集
        for (Laboratory conflictedLab : conflictedLabs) {
            labs.removeIf(laboratory -> laboratory.getLabNumber().equals(conflictedLab.getLabNumber()));
        }
        if (labs.size() == 0) {
            return false;
        }
        // 将可用实验室按照设备数量降序排序
        labs.sort((o1, o2) -> o2.getEquipmentCount() - o1.getEquipmentCount());
        // 判断当前实验室列表是否满足所需学生数量
        Integer readySize = 0;
        List<Laboratory> readyLabs = new ArrayList<>();
        for (Laboratory lab : labs) {
            if (readySize >= appliedStudentCount) {
                break;
            }
            readySize += lab.getEquipmentCount();
            readyLabs.add(lab);
        }
        // 当前实验室列表不满足所需学生数量
        if (readySize < appliedStudentCount) {
            return false;
        }
        for (Laboratory lab : readyLabs) {
            LabSchedule labSchedule = new LabSchedule();
            labSchedule.setLabNumber(lab.getLabNumber());
            labSchedule.setLabName(lab.getLabName());
            labSchedule.setSemesterId(semesterId);
            labSchedule.setTeacherAccount(appliedTeacherAccount);
            labSchedule.setTeacherName(appliedTeacherName);
            labSchedule.setStartWeek(appliedStartWeek);
            labSchedule.setEndWeek(appliedEndWeek);
            labSchedule.setDayOfWeek(appliedDayOfWeek);
            labSchedule.setTimeSlot(appliedTimeSlot);
            labSchedule.setCourseName(appliedCourseName);
            labSchedule.setStudentClass(appliedStudentClass);
            labScheduleMapper.insertLabSchedule(labSchedule);
        }
        // 修改申请状态为已排课
        LabClassApplication labClassApplication = new LabClassApplication();
        labClassApplication.setId(labApplicationDTO.getId());
        labClassApplication.setApplicationStatus("已排课");
        classApplicationMapper.update(labClassApplication);
        return true;
    }

    @Override
    public List<LabScheduleVO> getLabSchedule() {
        List<LabSchedule> labScheduleList = labScheduleMapper.getAllLabSchedules();
        List<LabScheduleVO> result = new ArrayList<>();
        for (LabSchedule labSchedule : labScheduleList) {
            LabScheduleVO labScheduleVO = new LabScheduleVO();
            labScheduleVO.setCourseName(labSchedule.getCourseName());
            labScheduleVO.setTeacherName(labSchedule.getTeacherName());
            labScheduleVO.setClassName(labSchedule.getStudentClass());
            labScheduleVO.setWeek(labSchedule.getStartWeek() + "-" + labSchedule.getEndWeek());
            labScheduleVO.setTimeSlot(labSchedule.getTimeSlot());
            labScheduleVO.setDayOfWeek(labSchedule.getDayOfWeek());
            labScheduleVO.setLabName(labSchedule.getLabName());
            labScheduleVO.setLabNumber(labSchedule.getLabNumber());
            result.add(labScheduleVO);
        }
        return result;
    }

    @Override
    public PageResult pageQuery(LabScheduleQueryDTO labScheduleQueryDTO) {
        PageHelper.startPage(labScheduleQueryDTO.getPage(), labScheduleQueryDTO.getPageSize());
        Page<LabScheduleQueryVO> page = labScheduleMapper.pageQuery(labScheduleQueryDTO);
        return new PageResult(page.getTotal(), page.getResult());
    }
}
