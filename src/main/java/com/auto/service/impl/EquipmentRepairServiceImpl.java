package com.auto.service.impl;

import com.auto.mapper.EquipmentRepairMapper;
import com.auto.mapper.LaboratoryMapper;
import com.auto.mapper.TeacherMapper;
import com.auto.pojo.dto.EquipmentRepairDTO;
import com.auto.pojo.dto.EquipmentRepairQueryDTO;
import com.auto.pojo.entity.EquipmentRepair;
import com.auto.pojo.entity.Laboratory;
import com.auto.pojo.entity.Semester;
import com.auto.pojo.entity.Teacher;
import com.auto.pojo.vo.ClassApplicationPageQueryVO;
import com.auto.pojo.vo.EquipmentRepairPageQueryVO;
import com.auto.result.PageResult;
import com.auto.service.EquipmentRepairService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class EquipmentRepairServiceImpl implements EquipmentRepairService {
    @Resource
    private EquipmentRepairMapper equipmentRepairMapper;
    @Resource
    private TeacherMapper teacherMapper;
    @Resource
    private LaboratoryMapper laboratoryMapper;

    @Transactional
    @Override
    public PageResult teacherEquipmentRepairPageQuery(EquipmentRepairQueryDTO equipmentRepairQueryDTO) {
        PageHelper.startPage(equipmentRepairQueryDTO.getPage(), equipmentRepairQueryDTO.getPageSize());
        Page<EquipmentRepairPageQueryVO> page = equipmentRepairMapper.pageQueryTeacher(equipmentRepairQueryDTO);

        List<EquipmentRepairPageQueryVO> result = page.getResult();
        Teacher teacher = teacherMapper.getTeacherByAccount(equipmentRepairQueryDTO.getAccount());
        for (EquipmentRepairPageQueryVO pageQueryVO : result) {
            pageQueryVO.setTeacher(teacher);
            pageQueryVO.setLaboratory(laboratoryMapper.getLaboratoryById(pageQueryVO.getLabId()));
        }
        return new PageResult(page.getTotal(), result);
    }

    @Override
    public void saveTeacher(EquipmentRepairDTO equipmentRepairDTO) {
        EquipmentRepair equipmentRepair = new EquipmentRepair();
        BeanUtils.copyProperties(equipmentRepairDTO, equipmentRepair);
        Laboratory laboratory = laboratoryMapper.getLaboratoryById(equipmentRepairDTO.getLabId());
        equipmentRepair.setExperimenterAccount(laboratory.getExperimenterAccount());
        equipmentRepair.setRepairDate(LocalDateTime.now());
        equipmentRepair.setRepairStatus("未维修");
        equipmentRepairMapper.insert(equipmentRepair);
    }

    @Transactional
    @Override
    public PageResult experimenterEquipmentRepairPageQuery(EquipmentRepairQueryDTO equipmentRepairQueryDTO) {
        PageHelper.startPage(equipmentRepairQueryDTO.getPage(), equipmentRepairQueryDTO.getPageSize());
        Page<EquipmentRepairPageQueryVO> page = equipmentRepairMapper.pageQueryExperimenter(equipmentRepairQueryDTO);

        List<EquipmentRepairPageQueryVO> result = page.getResult();

        for (EquipmentRepairPageQueryVO pageQueryVO : result) {
            String teacherAccount = equipmentRepairMapper.getTeacherAccountById(pageQueryVO.getId());
            pageQueryVO.setTeacher(teacherMapper.getTeacherByAccount(teacherAccount));
            pageQueryVO.setLaboratory(laboratoryMapper.getLaboratoryById(pageQueryVO.getLabId()));
        }
        return new PageResult(page.getTotal(), result);
    }

    @Override
    public void update(EquipmentRepairDTO equipmentRepairDTO) {
        EquipmentRepair equipmentRepair = new EquipmentRepair();
        BeanUtils.copyProperties(equipmentRepairDTO,equipmentRepair);
        equipmentRepairMapper.update(equipmentRepair);
    }
}
