package com.auto.service.impl;

import com.auto.mapper.LabBorrowApplicationMapper;
import com.auto.mapper.LaboratoryMapper;
import com.auto.mapper.SemesterMapper;
import com.auto.mapper.StudentMapper;
import com.auto.mapper.*;
import com.auto.pojo.dto.LabBorrowApplicationDTO;
import com.auto.pojo.dto.LabBorrowApplicationQueryDTO;
import com.auto.pojo.dto.LabBorrowDTO;
import com.auto.pojo.dto.StudentBorrowApplicationQueryDTO;
import com.auto.pojo.entity.LabBorrowApplication;
import com.auto.pojo.vo.LabBorrowApplicationPageVO;
import com.auto.result.PageResult;
import com.auto.pojo.entity.LabSchedule;
import com.auto.pojo.entity.Laboratory;
import com.auto.pojo.entity.Semester;
import com.auto.service.LabBorrowApplicationService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.BeanUtils;
import com.auto.service.LabScheduleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author WisdomX
 * @description: 实验室借用申请表接口实现类
 */
@Service
public class LabBorrowApplicationServiceImpl implements LabBorrowApplicationService {
    @Resource
    private LabScheduleMapper labScheduleMapper;
    @Resource
    private LabBorrowApplicationMapper labBorrowApplicationMapper;
    @Resource
    private LaboratoryMapper laboratoryMapper;
    @Resource
    private SemesterMapper semesterMapper;
    @Resource
    private StudentMapper studentMapper;


    @Override
    public List<Laboratory> getAvailableLabs(LabBorrowDTO labBorrowDTO) {
        Integer appliedWeekNumber = labBorrowDTO.getWeekNumber();
        Integer appliedDayOfWeek = labBorrowDTO.getDayOfWeek();
        String appliedTimeSlot = labBorrowDTO.getTimeSlot();
        List<Laboratory> allLabs = laboratoryMapper.getALLLabs();
        List<Laboratory> conflictLabs = new ArrayList<>();
        // 已安排实验室列表
        List<LabSchedule> labScheduledList = labScheduleMapper.getAllLabSchedules();
        // 已通过借用申请列表
        List<LabBorrowApplication> labBorrowedList = labBorrowApplicationMapper.getApprovedBorrowApplication();
        // 判断和已安排的实验室是否有冲突
        for (LabSchedule labSchedule : labScheduledList) {
            if (!(appliedWeekNumber >= labSchedule.getStartWeek() && appliedWeekNumber <= labSchedule.getEndWeek())) {
                continue;
            }
            if (appliedDayOfWeek != labSchedule.getDayOfWeek()) {
                continue;
            }
            if (!appliedTimeSlot.equals(labSchedule.getTimeSlot())) {
                continue;
            }
            conflictLabs.add(laboratoryMapper.getLaboratoryByNumber(labSchedule.getLabNumber()));
        }
        // 判断和已通过借用申请的实验室是否有冲突
        for (LabBorrowApplication application : labBorrowedList) {
            if (appliedWeekNumber != application.getWeekNumber()) {
                continue;
            }
            if (appliedDayOfWeek != application.getDayOfWeek()) {
                continue;
            }
            if (!appliedTimeSlot.equals(application.getTimeSlot())) {
                continue;
            }
            conflictLabs.add(laboratoryMapper.getLaboratoryById(application.getLabId()));
        }
        for (Laboratory lab : conflictLabs) {
            allLabs.removeIf(laboratory -> laboratory.getLabNumber().equals(lab.getLabNumber()));
        }
        // 将allLabs里的实验室按照实验室编号降序排序
        Collections.sort(allLabs, (o1, o2) -> o2.getLabNumber().compareTo(o1.getLabNumber()));
        return allLabs;
    }

    @Transactional
    @Override
    public PageResult pageQuery(LabBorrowApplicationQueryDTO labBorrowApplicationQueryDTO) {
        PageHelper.startPage(labBorrowApplicationQueryDTO.getPage(), labBorrowApplicationQueryDTO.getPageSize());
        Page<LabBorrowApplicationPageVO> page = labBorrowApplicationMapper.pageQuery(labBorrowApplicationQueryDTO);
        List<LabBorrowApplicationPageVO> result = page.getResult();
        for (LabBorrowApplicationPageVO labBorrowApplicationPageVO : result) {
            labBorrowApplicationPageVO.setStudent(studentMapper.getStudentByAccount(labBorrowApplicationPageVO.getStudentAccount()));
            labBorrowApplicationPageVO.setLaboratory(laboratoryMapper.getLaboratoryById(labBorrowApplicationPageVO.getLabId()));
            labBorrowApplicationPageVO.setSemester(semesterMapper.selectCurrentSemester());
        }
        return new PageResult(page.getTotal(), result);
    }

    @Override
    public void update(LabBorrowApplicationDTO labBorrowApplicationDTO) {
        LabBorrowApplication labBorrowApplication = new LabBorrowApplication();
        BeanUtils.copyProperties(labBorrowApplicationDTO, labBorrowApplication);
        labBorrowApplication.setApplicationDate(LocalDateTime.now());
        labBorrowApplicationMapper.update(labBorrowApplication);
    }

    @Override
    public void save(LabBorrowApplicationDTO labBorrowApplicationDTO) {
        LabBorrowApplication labBorrowApplication = new LabBorrowApplication();
        BeanUtils.copyProperties(labBorrowApplicationDTO, labBorrowApplication);
        labBorrowApplication.setApplicationDate(LocalDateTime.now());
        labBorrowApplicationMapper.insert(labBorrowApplication);
    }

    @Override
    public PageResult pageQueryStudent(StudentBorrowApplicationQueryDTO studentBorrowApplicationQueryDTO) {
        PageHelper.startPage(studentBorrowApplicationQueryDTO.getPage(), studentBorrowApplicationQueryDTO.getPageSize());
        Page<LabBorrowApplicationPageVO> page = labBorrowApplicationMapper.pageQueryStudent(studentBorrowApplicationQueryDTO);
        List<LabBorrowApplicationPageVO> result = page.getResult();
        for (LabBorrowApplicationPageVO labBorrowApplicationPageVO : result) {
            labBorrowApplicationPageVO.setStudent(studentMapper.getStudentByAccount(labBorrowApplicationPageVO.getStudentAccount()));
            labBorrowApplicationPageVO.setLaboratory(laboratoryMapper.getLaboratoryById(labBorrowApplicationPageVO.getLabId()));
            labBorrowApplicationPageVO.setSemester(semesterMapper.selectCurrentSemester());
        }
        return new PageResult(page.getTotal(), result);
    }
}
