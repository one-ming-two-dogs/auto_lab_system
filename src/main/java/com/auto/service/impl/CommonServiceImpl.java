package com.auto.service.impl;

import com.auto.constant.MessageConstant;
import com.auto.exception.BulkImportException;
import com.auto.mapper.ExperimenterMapper;
import com.auto.mapper.StudentMapper;
import com.auto.mapper.TeacherMapper;
import com.auto.pojo.entity.Experimenter;
import com.auto.pojo.entity.Student;
import com.auto.pojo.entity.Teacher;
import com.auto.service.CommonService;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;

import javax.annotation.Resource;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * @author: LoungeXi
 * @description: 通用接口实现类
 */
@Slf4j
@Service
public class CommonServiceImpl implements CommonService {

    @Resource
    private StudentMapper studentMapper;
    @Resource
    private TeacherMapper teacherMapper;
    @Resource
    private ExperimenterMapper experimenterMapper;

    @Transactional
    @Override
    public String bulkImport(MultipartFile file) {
        return dealFile(file);
    }


    private String dealFile(MultipartFile file) {
        if (file == null ) {
            log.info("文件错误状态:{}", MessageConstant.FILE_NOT_FOUND);
            throw new BulkImportException(MessageConstant.FILE_NOT_FOUND);
        }
        String fileName = file.getOriginalFilename();
        if (fileName == null) {
            log.info("文件错误状态:{}", MessageConstant.UNKNOWN_ERROR);
            throw new BulkImportException(MessageConstant.UNKNOWN_ERROR);
        }
        String fileType = fileName.substring(fileName.lastIndexOf(".") + 1);
        try {
            InputStream fileInputStream = file.getInputStream();
            Workbook workbook = null;
            if (fileType.equalsIgnoreCase("xls")) {
                workbook = new HSSFWorkbook(fileInputStream); //生成.xls的excel
            } else if (fileType.equalsIgnoreCase("xlsx")) {
                workbook = new XSSFWorkbook(fileInputStream); //生成.xlsx的excel
            } else {
                log.info("文件错误状态:{}", MessageConstant.FILE_FORMAT_ERR);
                throw new BulkImportException(MessageConstant.FILE_FORMAT_ERR);
            }
            Boolean result = encapsulate(workbook);
            if (!result) {
                return MessageConstant.USER_TYPE_ERR;
            }
        } catch (Exception e) {
            throw new BulkImportException(MessageConstant.UNKNOWN_ERROR);
        }
        return MessageConstant.IMPORT_SUCCESS;
    }

    private Boolean encapsulate(Workbook workbook) {
        Sheet sheet = workbook.getSheetAt(0);
        int index = 0;
        Row firstRow = sheet.getRow(index);
        String userType = convertCellValueToString(firstRow.getCell(1));
        log.info("用户类型:{}", userType);
        return importData(index, sheet, userType);
    }

    private Boolean importData(int index, Sheet sheet, String userType) {
        index = index + 2;
        for (int i = index; ; i++) {
            Row row = sheet.getRow(i);
            if (row == null) {
                break;
            }
            switch (userType) {
                case "学生": {
                    List<String> messageList = getMessageList(row);
                    Student student = new Student();
                    student.setAccount(messageList.get(1));
                    student.setName(messageList.get(2));
                    student.setMajor(messageList.get(3));
                    student.setClassNumber(messageList.get(4));
                    student.setPassWord(MessageConstant.DEFAULT_PASSWORD);
                    log.info("插入学生数据:{}", student);
                    studentMapper.insert(student);
                    break;
                }
                case "教师": {
                    List<String> messageList = getMessageList(row);
                    Teacher teacher = new Teacher();
                    teacher.setAccount(messageList.get(1));
                    teacher.setName(messageList.get(2));
                    teacher.setJobTitle(messageList.get(5));
                    teacher.setPassWord(MessageConstant.DEFAULT_PASSWORD);
                    log.info("插入教师数据:{}", teacher);
                    teacherMapper.insert(teacher);
                    break;
                }
                case "实验员": {
                    List<String> messageList = getMessageList(row);
                    Experimenter experimenter = new Experimenter();
                    experimenter.setAccount(messageList.get(1));
                    experimenter.setName(messageList.get(2));
                    experimenter.setJobTitle(messageList.get(5));
                    experimenter.setPassWord(MessageConstant.DEFAULT_PASSWORD);
                    log.info("插入实验员数据:{}", experimenter);
                    experimenterMapper.insert(experimenter);
                    break;
                }
                default:
                    return false;
            }
        }
        return true;
    }

    private List<String> getMessageList(Row row) {
        List<String> messageList = new ArrayList<>();
        for (int j = 0; ; j++) {
            String string = convertCellValueToString(row.getCell(j));
            if (string == null && j > 5) {
                break;
            }
            messageList.add(string);
        }
        return messageList;
    }

    private String convertCellValueToString(Cell cell) {
        if (cell == null) {
            return null;
        }
        String content = null;
        try {
            switch (cell.getCellType()) {
                case NUMERIC:   //数字或者时间
                    Double doubleValue = cell.getNumericCellValue();
                    // 格式化科学计数法，取一位整数
                    DecimalFormat df = new DecimalFormat("0");
                    content = df.format(doubleValue);
                    break;
                case STRING:    //字符串
                    content = cell.getStringCellValue();
                    break;
                case BOOLEAN:   //布尔
                    Boolean booleanValue = cell.getBooleanCellValue();
                    content = booleanValue.toString();
                    break;
                case BLANK:     // 空值
                    break;
                case FORMULA:   // 公式
                    content = cell.getCellFormula();
                    break;
                case ERROR:     // 故障
                    break;
                default:
                    break;
            }
        } catch (Exception e) {
            throw new BulkImportException(MessageConstant.UNKNOWN_ERROR);
        }
        return content;
    }


}
