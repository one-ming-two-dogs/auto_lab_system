package com.auto.service.impl;

import com.auto.constant.MessageConstant;
import com.auto.exception.AccountNotFoundException;
import com.auto.exception.PasswordErrorException;
import com.auto.mapper.AdministratorMapper;
import com.auto.pojo.dto.UserLoginDTO;
import com.auto.pojo.entity.Administrator;
import com.auto.service.AdministratorService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author LoungeXi
 * @description 系统管理员接口实现类
 */
@Service
public class AdministratorServiceImpl implements AdministratorService {

    @Resource
    private AdministratorMapper administratorMapper;

    @Override
    public Administrator login(UserLoginDTO userLoginDTO) {
        String account = userLoginDTO.getAccount();

        Administrator administrator = administratorMapper.getAdministratorByAccount(account);

        if (administrator == null) {
            throw new AccountNotFoundException(MessageConstant.ACCOUNT_NOT_FOUND);
        }

        String passWord = userLoginDTO.getPassWord();
        if(!administrator.getPassWord().equals(passWord)){
            throw new PasswordErrorException(MessageConstant.PASSWORD_ERROR);
        }

        return administrator;
    }
}
