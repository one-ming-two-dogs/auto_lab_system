package com.auto.service.impl;

import com.auto.context.SemesterContext;
import com.auto.mapper.SemesterMapper;
import com.auto.pojo.dto.SemesterDTO;
import com.auto.pojo.entity.Semester;
import com.auto.pojo.vo.SemesterVO;
import com.auto.service.SemesterService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @author LoungeXi
 * @description 学期接口实现类
 */
@Slf4j
@Service
public class SemesterServiceImpl implements SemesterService {
    @Resource
    private SemesterMapper semesterMapper;

    @Override
    public List<SemesterVO> selectAllSemester() {
        List<Semester> semesterslist = semesterMapper.selectAllSemester();
        List<SemesterVO> resultList = new ArrayList<>();
        for (Semester semester : semesterslist) {
            SemesterVO semesterVO = new SemesterVO();
            semesterVO.setId(semester.getId());
            semesterVO.setStartYear(semester.getStartYear());
            semesterVO.setEndYear(semester.getEndYear());
            semesterVO.setQuarter(semester.getQuarter());
            semesterVO.setSemester(semester.getStartYear() + "-" + semester.getEndYear() + "-" + semester.getQuarter());
            resultList.add(semesterVO);
        }
        return resultList;
    }

    @Transactional
    @Override
    public void setCurrentSemester(SemesterDTO semesterDTO) {
        semesterMapper.setOtherSemester();
        String[] split = semesterDTO.getCurrentSemester().split("-");
        Semester semester = new Semester();
        semester.setStartYear(Integer.parseInt(split[0]));
        semester.setEndYear(Integer.parseInt(split[1]));
        semester.setQuarter(Integer.parseInt(split[2]));
        semester.setIsCurrentSemester(Boolean.TRUE);
        semesterMapper.setCurrentSemester(semester);
        log.info("设置前:{}", SemesterContext.CURRENT_SEMESTER_ID);
        SemesterContext.CURRENT_SEMESTER_ID = semesterMapper.selectCurrentSemester().getId();
        log.info("设置后:{}", SemesterContext.CURRENT_SEMESTER_ID);
    }

    @Override
    public Semester selectCurrentSemester() {
        return semesterMapper.selectCurrentSemester();
    }
}
