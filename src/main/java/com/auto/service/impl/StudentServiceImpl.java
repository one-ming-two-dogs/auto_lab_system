package com.auto.service.impl;

import com.auto.constant.MessageConstant;
import com.auto.exception.AccountNotFoundException;
import com.auto.exception.PasswordErrorException;
import com.auto.mapper.StudentMapper;
import com.auto.pojo.dto.StudentDTO;
import com.auto.pojo.dto.UserLoginDTO;
import com.auto.pojo.dto.UserQueryDTO;
import com.auto.pojo.entity.Student;
import com.auto.pojo.entity.Teacher;
import com.auto.pojo.vo.StudentPageQueryVO;
import com.auto.result.PageResult;
import com.auto.service.StudentService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author LoungeXi
 * @description 学生接口实现类
 */
@Service
public class StudentServiceImpl implements StudentService {

    @Resource
    private StudentMapper studentMapper;


    @Override
    public PageResult pageQuery(UserQueryDTO userQueryDTO) {
        PageHelper.startPage(userQueryDTO.getPage(), userQueryDTO.getPageSize());
        Page<StudentPageQueryVO> page = studentMapper.pageQuery(userQueryDTO);
        return new PageResult(page.getTotal(), page.getResult());
    }

    @Override
    public void save(StudentDTO studentDTO) {
        Student student = new Student();
        BeanUtils.copyProperties(studentDTO, student);
        studentMapper.insert(student);
    }

    @Override
    public void deleteByAccount(String account) {
        studentMapper.deleteById(account);
    }

    @Override
    public void update(StudentDTO studentDTO) {
        Student student = new Student();
        BeanUtils.copyProperties(studentDTO, student);
        studentMapper.update(student);
    }

    @Override
    public Student login(UserLoginDTO userLoginDTO) {
        String account = userLoginDTO.getAccount();

        Student student = studentMapper.getStudentByAccount(account);

        if (student == null) {
            throw new AccountNotFoundException(MessageConstant.ACCOUNT_NOT_FOUND);
        }

        String passWord = userLoginDTO.getPassWord();
        if(!student.getPassWord().equals(passWord)){
            throw new PasswordErrorException(MessageConstant.PASSWORD_ERROR);
        }

        return student;
    }
}
