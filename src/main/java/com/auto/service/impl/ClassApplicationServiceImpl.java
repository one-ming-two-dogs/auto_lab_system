package com.auto.service.impl;

import com.auto.constant.MessageConstant;
import com.auto.exception.ApplicationStatusErrorException;
import com.auto.mapper.ClassApplicationMapper;
import com.auto.mapper.SemesterMapper;
import com.auto.mapper.TeacherMapper;
import com.auto.pojo.dto.ClassApplicationDTO;
import com.auto.pojo.dto.ClassApplicationQueryDTO;
import com.auto.pojo.entity.LabClassApplication;
import com.auto.pojo.entity.Semester;
import com.auto.pojo.entity.Teacher;
import com.auto.pojo.vo.ClassApplicationPageQueryVO;
import com.auto.pojo.vo.StudentPageQueryVO;
import com.auto.result.PageResult;
import com.auto.service.ClassApplicationService;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author LoungeXi
 * @description 课程申请接口实现类
 */
@Slf4j
@Service
public class ClassApplicationServiceImpl implements ClassApplicationService {
    @Resource
    private ClassApplicationMapper classApplicationMapper;
    @Resource
    private TeacherMapper teacherMapper;
    @Resource
    private SemesterMapper semesterMapper;

    @Transactional
    @Override
    public PageResult pageQuery(ClassApplicationQueryDTO classApplicationQueryDTO) {
        PageHelper.startPage(classApplicationQueryDTO.getPage(), classApplicationQueryDTO.getPageSize());
        Page<ClassApplicationPageQueryVO> page = classApplicationMapper.pageQuery(classApplicationQueryDTO);
        List<ClassApplicationPageQueryVO> result = page.getResult();
        Teacher teacher = teacherMapper.getTeacherByAccount(classApplicationQueryDTO.getAccount());
        Semester semester = semesterMapper.selectCurrentSemester();
        for (ClassApplicationPageQueryVO pageQueryVO : result) {
            pageQueryVO.setTeacher(teacher);
            pageQueryVO.setSemester(semester);
        }
        return new PageResult(page.getTotal(), result);
    }

    @Override
    public void save(ClassApplicationDTO classApplicationDTO) {
        LabClassApplication labClassApplication = new LabClassApplication();
        BeanUtils.copyProperties(classApplicationDTO, labClassApplication);
        labClassApplication.setApplicationStatus("未排课");
        classApplicationMapper.insert(labClassApplication);
    }

    @Transactional
    @Override
    public void update(ClassApplicationDTO classApplicationDTO) {
        if (!classApplicationMapper.selectById(classApplicationDTO.getId()).equals("未排课")) {
            throw new ApplicationStatusErrorException(MessageConstant.APPLICATION_STATUS_ERROR);
        }
        LabClassApplication labClassApplication = new LabClassApplication();
        BeanUtils.copyProperties(classApplicationDTO, labClassApplication);
        classApplicationMapper.update(labClassApplication);
    }

    @Transactional
    @Override
    public void deleteById(Integer id) {
        if (!classApplicationMapper.selectById(id).equals("未排课")) {
            throw new ApplicationStatusErrorException(MessageConstant.APPLICATION_STATUS_ERROR);
        }
        classApplicationMapper.deleteById(id);
    }


}
