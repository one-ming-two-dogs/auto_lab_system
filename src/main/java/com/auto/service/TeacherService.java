package com.auto.service;

import com.auto.pojo.dto.ExperimenterOrTeacherDTO;
import com.auto.pojo.dto.UserLoginDTO;
import com.auto.pojo.dto.UserQueryDTO;
import com.auto.pojo.entity.Teacher;
import com.auto.result.PageResult;

/**
 * @author LoungeXi
 * @date: 2024/4/13 17:21
 * @description 教师接口
 */
public interface TeacherService {
    PageResult pageQuery(UserQueryDTO userQueryDTO);

    void save(ExperimenterOrTeacherDTO experimenterOrTeacherDTO);

    void deleteByAccount(String account);

    void update(ExperimenterOrTeacherDTO experimenterOrTeacherDTO);

    Teacher login(UserLoginDTO userLoginDTO);
}
