package com.auto.service;

import com.auto.pojo.dto.StudentDTO;
import com.auto.pojo.dto.UserLoginDTO;
import com.auto.pojo.dto.UserQueryDTO;
import com.auto.pojo.entity.Student;
import com.auto.result.PageResult;

/**
 * @author: LoungeXi
 * @date: 2024/4/12 21:03
 * @description: 学生接口
 */
public interface StudentService {


    PageResult pageQuery(UserQueryDTO userQueryDTO);

    void save(StudentDTO studentDTO);

    void deleteByAccount(String account);

    void update(StudentDTO studentDTO);

    Student login(UserLoginDTO userLoginDTO);
}
