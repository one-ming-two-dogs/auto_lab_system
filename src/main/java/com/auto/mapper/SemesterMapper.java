package com.auto.mapper;

import com.auto.pojo.entity.Semester;
import com.auto.pojo.vo.SemesterVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * @author: LoungeXi
 * @date: 2024/4/13 22:32
 * @description: 学期Mapper接口
 */
@Mapper
public interface SemesterMapper {
    @Select("select id,start_year,end_year,quarter from Semesters")
    List<Semester> selectAllSemester();

    @Update("update Semesters set is_current_semester = FALSE")
    void setOtherSemester();

    void setCurrentSemester(Semester semester);

    @Select("select id,start_year,end_year,quarter from Semesters where is_current_semester = True")
    Semester selectCurrentSemester();
}
