package com.auto.mapper;

import com.auto.pojo.entity.Administrator;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

/**
 * @author: LoungeXi
 * @date: 2024/4/13 19:45
 * @description: 系统管理员Mapper接口
 */
@Mapper
public interface AdministratorMapper {
    @Select("select * from Administrators where account = #{account}")
    Administrator getAdministratorByAccount(String account);
}
