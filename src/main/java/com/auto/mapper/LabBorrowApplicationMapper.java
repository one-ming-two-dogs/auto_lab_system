package com.auto.mapper;

import com.auto.pojo.dto.LabBorrowApplicationDTO;
import com.auto.pojo.dto.LabBorrowApplicationQueryDTO;
import com.auto.pojo.dto.StudentBorrowApplicationQueryDTO;
import com.auto.pojo.entity.LabBorrowApplication;
import com.auto.pojo.vo.LabBorrowApplicationPageVO;
import com.github.pagehelper.Page;
import org.apache.ibatis.annotations.Insert;
import com.auto.pojo.entity.Laboratory;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

@Mapper
public interface LabBorrowApplicationMapper {
    @Update("update Lab_Borrow_Applications set application_status = #{applicationStatus} where id = #{id}")
    void approvalBorrowApplication(LabBorrowApplication labBorrowApplication);

    @Select("select * from Lab_Borrow_Applications where semester_id = (select id from Semesters where is_current_semester = True) and application_status = '通过'")
    List<LabBorrowApplication> getApprovedBorrowApplication();

    Page<LabBorrowApplicationPageVO> pageQuery(LabBorrowApplicationQueryDTO labBorrowApplicationQueryDTO);

    void update(LabBorrowApplication labBorrowApplication);

    @Insert("INSERT INTO Lab_Borrow_Applications (student_account, lab_id, semester_id, week_number, day_of_week, time_slot, reason, application_status, application_date) VALUE (#{studentAccount},#{labId},#{semesterId},#{weekNumber},#{dayOfWeek},#{timeSlot},#{reason},#{applicationStatus},#{applicationDate})")
    void insert(LabBorrowApplication labBorrowApplication);

    Page<LabBorrowApplicationPageVO> pageQueryStudent(StudentBorrowApplicationQueryDTO studentBorrowApplicationQueryDTO);
}
