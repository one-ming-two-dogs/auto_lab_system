package com.auto.mapper;

import com.auto.pojo.dto.UserQueryDTO;
import com.auto.pojo.entity.Experimenter;
import com.auto.pojo.vo.ExperimenterOrTeacherPageQueryVO;
import com.github.pagehelper.Page;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

/**
 * @author: LoungeXi
 * @date: 2024/4/13 17:30
 * @description: 实验员Mapper接口
 */
@Mapper
public interface ExperimenterMapper {
    Page<ExperimenterOrTeacherPageQueryVO> pageQuery(UserQueryDTO userQueryDTO) ;

    @Insert("insert into Experimenters (account, name, job_title, password) VALUE (#{account},#{name},#{jobTitle},#{passWord})")
    void insert(Experimenter experimenter);

    @Delete("delete from Experimenters where account = #{account}")
    void deleteByAccount(String account);

    void update(Experimenter experimenter);

    @Select("select * from Experimenters where account = #{account}")
    Experimenter getExperimenterByAccount(String account);
}
