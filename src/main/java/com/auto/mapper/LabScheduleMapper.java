package com.auto.mapper;

import com.auto.pojo.dto.LabScheduleQueryDTO;
import com.auto.pojo.entity.LabSchedule;
import com.auto.pojo.vo.LabScheduleQueryVO;
import com.auto.result.PageResult;
import com.github.pagehelper.Page;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface LabScheduleMapper {
    @Select("select * from Lab_Schedules where semester_id = (select id from Semesters where is_current_semester = True)")
    List<LabSchedule> getAllLabSchedules();

    @Insert("insert into Lab_Schedules (semester_id, teacher_account, teacher_name, lab_number, lab_name, start_week, end_week, day_of_week, time_slot, course_name, student_class) VALUE (#{semesterId},#{teacherAccount},#{teacherName},#{labNumber},#{labName},#{startWeek},#{endWeek},#{dayOfWeek},#{timeSlot},#{courseName},#{studentClass})")
    Boolean insertLabSchedule(LabSchedule labSchedule);

    Page<LabScheduleQueryVO> pageQuery(LabScheduleQueryDTO labScheduleQueryDTO);
}
