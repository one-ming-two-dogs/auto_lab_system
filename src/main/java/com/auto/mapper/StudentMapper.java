package com.auto.mapper;

import com.auto.pojo.dto.StudentDTO;
import com.auto.pojo.dto.UserQueryDTO;
import com.auto.pojo.entity.Student;
import com.auto.pojo.vo.StudentPageQueryVO;
import com.github.pagehelper.Page;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

/**
 * @author: LoungeXi
 * @date: 2024/4/13 13:50
 * @description: 学生Mapper接口
 */
@Mapper
public interface StudentMapper {

    Page<StudentPageQueryVO> pageQuery(UserQueryDTO userQueryDTO);

    @Insert("insert into Students (account, name, major, class_number, password) value (#{account},#{name},#{major},#{classNumber},#{passWord})")
    void insert(Student student);

    @Delete("delete from Students where account = #{account}")
    void deleteById(String account);

    void update(Student student);

    @Select("select * from Students where account = #{account}")
    Student getStudentByAccount(String account);
}
