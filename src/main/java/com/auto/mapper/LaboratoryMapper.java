package com.auto.mapper;

import com.auto.pojo.entity.Laboratory;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author: LoungeXi
 * @date: 2024/4/13 17:30
 * @description: 实验室Mapper接口
 */
@Mapper
public interface LaboratoryMapper {
    @Select("SELECT id, experimenter_account, experimenter_name, lab_number, lab_name, lab_type, equipment_count FROM Labs WHERE id = #{laboratoryId};")
    Laboratory getLaboratoryById(Integer laboratoryId);

    @Select("select * from Labs where lab_number = #{labNumber};")
    Laboratory getLaboratoryByNumber(String labNumber);

    @Select("SELECT * FROM Labs WHERE lab_type = #{labType};")
    List<Laboratory> listLaboratoriesByType(String labType);

    @Select("select * from Labs")
    List<Laboratory> getALLLabs();
}
