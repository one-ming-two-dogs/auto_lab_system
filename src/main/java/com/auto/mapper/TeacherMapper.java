package com.auto.mapper;

import com.auto.pojo.dto.UserQueryDTO;
import com.auto.pojo.entity.Teacher;
import com.auto.pojo.vo.ExperimenterOrTeacherPageQueryVO;
import com.github.pagehelper.Page;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

/**
 * @author: LoungeXi
 * @date: 2024/4/13 17:29
 * @description: 教师Mapper接口
 */
@Mapper
public interface TeacherMapper {
    Page<ExperimenterOrTeacherPageQueryVO> pageQuery(UserQueryDTO userQueryDTO) ;

    @Insert("insert into Teachers (account, name, job_title, password) VALUE (#{account},#{name},#{jobTitle},#{passWord})")
    void insert(Teacher teacher);

    @Delete("delete from Teachers where account = #{account}")
    void deleteByAccount(String account);

    void update(Teacher teachers);

    @Select("select * from Teachers where account = #{account}")
    Teacher getTeacherByAccount(String account);
}
