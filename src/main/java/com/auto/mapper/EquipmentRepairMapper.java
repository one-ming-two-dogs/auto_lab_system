package com.auto.mapper;

import com.auto.pojo.dto.EquipmentRepairQueryDTO;
import com.auto.pojo.entity.EquipmentRepair;
import com.auto.pojo.entity.Teacher;
import com.auto.pojo.vo.EquipmentRepairPageQueryVO;
import com.github.pagehelper.Page;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

/**
 * @author: LoungeXi
 * @date: 2024/4/13 17:30
 * @description: 报修Mapper接口
 */
@Mapper
public interface EquipmentRepairMapper {
    @Insert("INSERT INTO Equipment_Repairs (teacher_account , experimenter_account, lab_id, issue_description,repair_date,repair_status) VALUES (#{teacherAccount},#{experimenterAccount},#{labId},#{issueDescription},#{repairDate},#{repairStatus})")
    void insert(EquipmentRepair equipmentRepair);

    Page<EquipmentRepairPageQueryVO> pageQueryTeacher(EquipmentRepairQueryDTO equipmentRepairQueryDTO);

    Page<EquipmentRepairPageQueryVO> pageQueryExperimenter(EquipmentRepairQueryDTO equipmentRepairQueryDTO);

    @Select("SELECT teacher_account from Equipment_Repairs WHERE id = #{id}")
    String getTeacherAccountById(Integer id);

    void update(EquipmentRepair equipmentRepair);
}
