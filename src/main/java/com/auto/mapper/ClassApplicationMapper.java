package com.auto.mapper;

import com.auto.pojo.dto.ClassApplicationQueryDTO;
import com.auto.pojo.entity.LabClassApplication;
import com.auto.pojo.vo.ClassApplicationPageQueryVO;
import com.auto.pojo.vo.StudentPageQueryVO;
import com.github.pagehelper.Page;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

/**
 * @author: LoungeXi
 * @date: 2024/4/13 19:45
 * @description: 课程申请Mapper接口
 */

@Mapper
public interface ClassApplicationMapper {
    Page<ClassApplicationPageQueryVO> pageQuery(ClassApplicationQueryDTO classApplicationQueryDTO);

    @Insert("insert into Lab_Class_Applications (semester_id, teacher_account, teacher_name, course_name, lab_type,student_class, student_count, start_week, end_week, day_of_week, time_slot,application_status) value (#{semesterId},#{teacherAccount},#{teacherName},#{courseName},#{labType},#{studentClass},#{studentCount},#{startWeek},#{endWeek},#{dayOFWeek},#{timeSlot},#{applicationStatus})")
    void insert(LabClassApplication labClassApplication);

    @Select("select application_status from Lab_Class_Applications where id = #{id}")
    String selectById(Integer id);

    void update(LabClassApplication labClassApplication);

    @Delete("delete from Lab_Class_Applications where id = #{id}")
    void deleteById(Integer id);
}
