package com.auto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class AutoLabSystemApplication {

    public static void main(String[] args) {
        SpringApplication.run(AutoLabSystemApplication.class, args);
    }

}
