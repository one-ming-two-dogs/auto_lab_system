package com.auto.exception;

/**
 * @author LoungeXi
 * @description 业务异常顶层类
 */
public class BaseException extends RuntimeException{
    public BaseException() {
    }

    public BaseException(String msg) {
        super(msg);
    }
}
