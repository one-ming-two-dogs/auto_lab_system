package com.auto.exception;

/**
 * @author LoungeXi
 * @description 申请表异常类
 */
public class ApplicationStatusErrorException extends BaseException{
    public ApplicationStatusErrorException() {
    }

    public ApplicationStatusErrorException(String msg) {
        super(msg);
    }
}
