package com.auto.exception;

/**
 * @author LoungeXi
 * @description 密码错误异常类
 */
public class PasswordErrorException extends BaseException{
    public PasswordErrorException() {
    }

    public PasswordErrorException(String msg) {
        super(msg);
    }
}
