package com.auto.exception;

/**
 * @author LoungeXi
 * @description 账号不存在异常类
 */
public class AccountNotFoundException extends BaseException {
    public AccountNotFoundException() {
    }

    public AccountNotFoundException(String msg) {
        super(msg);
    }
}
