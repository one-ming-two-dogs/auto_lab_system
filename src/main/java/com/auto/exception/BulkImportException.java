package com.auto.exception;
/**
 * @author LoungeXi
 * @description 批量导入异常类
 */
public class BulkImportException extends BaseException{

    public BulkImportException() {
    }

    public BulkImportException(String msg) {
        super(msg);
    }
}
